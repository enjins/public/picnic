# Picnic Hackathon

Creativity always beats brute-force. We mimic how our brains look at the pictures, namely: if there's a label, let's read the label. If not: can we recognize the item?

## Introduction

In this repository you can find all the necessary files for running our scripts for the [Picnic Hackathon challenge](https://picnic.devpost.com/). The challenge proposed is the following: *As a first step in helping customer support, come up with a way of labeling every picture that comes in according to the product that is in the picture. To keep with the Picnic spirit, we encourage to be as innovative and creative with your solutions as possible.*

- [Picnic Hackathon](#picnic-hackathon)
  * [Introduction](#introduction)
  * [How it works](#how-it-works)
    + [Data analysis](#data-analysis)
    + [Tesseract](#tesseract)
    + [CNN](#cnn)
    + [Combination of Tesseract & CNN](#combination-of-tesseract---cnn)
  * [Predictions](#predictions)
  * [Possible in about 2 weeks](#possible-in-about-2-weeks)
  * [Possible in about 2 months](#possible-in-about-2-months)
  * [Out-of-the-box solution](#out-of-the-box-solution)

## How it works

Our methodology is illustrated by the following figure. To start of, we try to extract text from the images by using a library called Tesseract. After filtering the images with text, the remaining images are sent to the pre-trained CNN that uses the VGG-19 model as a base and by adding a few extra layers of our own, we can make the net a better fit for the specific images in the dataset. 

![Picnic model flow](Picnic-workflow.png)

### Data analysis
Before we train the models we started of with [data analysis](https://gitlab.com/enjins/open/picnic/blob/master/data_analysis/Data_analysis_notebook.ipynb). This includes inspecting the pictures, checking the distribution of each class, finding patterns in the pictures. Several reasons are:
1. Inspecting the content of the pictures is helpful for choosing the right CNN strategy (pre-trained network, transfer learning, or a model from scratch),
2. Knowing the distribution of the classes gives some information for up- or downscaling,
3. Getting a feeling for the content of the pictures may spark your creative imagination,

#### Observations from distribution
Not all classes are equally balanced. But there is no extreme inbalance. Balancing might help the accuracy but could also lower the accuracy if the photos are of a bad quality and are upscaled by using image augmentation tecniques such as rotation, zoom, flip, etc. Secondly, Class balancing techniques are only really necessary when we actually care about the minority classes. Before training the model it is interesting to inspect the picture manualy

From some small examples we can see that there are some pictures with labels as a sticker. This suggests that we can read the text to maybe identify the content of the picture by analyzing the text instead of classification by a CNN. Secondly we observe that there are some classes that have multiple different products so, mapping based on training ourselves can be usefull. Thirdly there are trained CNN's available that we can use to speed up the process of learning by using a method called transfer learning.

### Tesseract
Since we learned that reading images improved performance, we started of building a model to read labels. This type of model is called Tesseract. Code can be found [here](https://gitlab.com/enjins/open/picnic/blob/master/tesseract/tess_predict.py).

Advantages:
- Labels with clear text often contain relevant information such as the product name. By reading this the overall accuracy can improve
- By applying the Tesseract before the CNN, images that don't contain the product itself but only the label are removed and thus the remaining set will be cleaner. 

Points for improvement:
- Flip images to better read text
- If an image is to dark, try to increase the brightness 
- Words need to be assigned to the labels. The current words are translations but could be improved by including other product names in the list. An example could be: `Gebraden Rosbief --> Lunch and delimeats`

### CNN
For the remaining photos we used a transfer learning method that uses a pre-trained model. In our case the VGG-19 model performed the best. Besides VGG-19, the VGG-16 and Resnet-50 were also used but underperformed. After epoch 12 the model stopped improving. The accuracy on the validation set was 57.44% as seen below. Code can be found [here](https://gitlab.com/enjins/open/picnic/blob/master/vgg19/train_vgg19.py).

```
Epoch 12/100
205/204 [==============================] - 10262s 50s/step - loss: 1.3046 - acc: 0.6256 - val_loss: 1.6910 - val_acc: 0.5744Epoch 00012: val_acc improved from 0.57025 to 0.57438, saving model to vgg19-model2.
```

### Combination of Tesseract & CNN
The tesseract was able to read text in 5% of all the pictures in the training set. The accuracy was 83% on the training setCombining models. Because the CNN is trained on all photos in the training set. The accuracy percentage of the combination is not possible to give. The reason being that we would run the model on a different train test split and therefore test on photos that were previously used for training. Our estimation for the performance would be between 57% - 65%.

## Predictions
The solution is combined in the [predictor script](https://gitlab.com/enjins/open/picnic/blob/master/predictor.py). It combines Tesseract with our trained CNN. In order to use our solution, look at the [example script](https://gitlab.com/enjins/open/picnic/blob/master/example.py). Our submission is included in the [TSV file](https://gitlab.com/enjins/open/picnic/blob/master/submission.tsv).

## Possible in about 2 weeks
There are plenty of relatively easy improvements when having more time. Among them:
- Image augmentation to enhance the training data
- Experimenting with changing layers in the network
- Balance classes more equally

## Possible in about 2 months
Develop an architecture to deploy the model in real life by:
- Contairize project and deploy on Kubernetes cluster
- Setup CI/CD environment
- Model monitoring, in order to learn and auto-improve the models

## Out-of-the-box solution
During the analysis we noticed that the quality of photos could be improved. One way could be to let users send their photos through the app. When using the camera, users can see the label that is predicted by the CNN. If correct, the user can confirm this and thereby improving the quality of the data and adding more labeled data to the total dataset.

The Enjins team!
https://enjins.com/ 
