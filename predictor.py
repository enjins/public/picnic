'''
Project:        The Picnic Hackathon
File:           predictor.py
Description:    Main application file combining different methods to label an image
Author:         Lars Suanet <lsuanet@enjins.com>
Company:		Enjins B.V.
Creation date:  29/03/2019
Last modified:  11/04/2019
'''

# import ML and processing libs
from keras.models import model_from_json
from keras.preprocessing.image import load_img, img_to_array
from keras.applications.vgg19 import preprocess_input
import numpy as np

# import custom libs
from tesseract import TesseractPredict

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

MODEL_PATH = 'vgg19/vgg19_model.json'
WEIGHTS_PATH = 'vgg19/vgg19_model.h5'

class PicnicPredictor():
	'''Class for predicting images for the Picnic hackathon

	'''

	def __init__(self,verbose=False):
		'''

		:param verbose: whether to print output
		'''

		self.__verbose = verbose

		# init Tesseract
		self.__tesseract = TesseractPredict()
		if self.__verbose:
			print("Initialised tesseract")

		# init VGG19 model
		with open(MODEL_PATH, 'r') as model:
			loaded_model_json = model.read()
		loaded_model = model_from_json(loaded_model_json)
		loaded_model.load_weights(WEIGHTS_PATH)
		self.__vgg19model = loaded_model
		if self.__verbose:
			print("Loaded model VGG19 from disk")

		self.__labels = ['Asparagus, string beans & brussels sprouts', 'Bananas, apples & pears',
					'Bell peppers, zucchinis & eggplants', 'Berries & cherries',
					'Broccoli, cauliflowers, carrots & radish', 'Cheese', 'Citrus fruits',
					'Cucumber, tomatoes & avocados', 'Eggs', 'Fish', 'Fresh bread',
					'Fresh herbs', 'Kiwis, grapes & mango', 'Lunch & Deli Meats', 'Milk',
					'Minced meat & meatballs', 'Nectarines, peaches & apricots',
					'Onions, leek, garlic & beets', 'Pineapples, melons & passion fruit',
					'Pork, beef & lamb', 'Potatoes', 'Poultry', 'Pre-baked breads',
					'Pudding, yogurt & quark', 'Salad & cress']

		return

	def __onehot_to_label(self, onehot):
		''' Translates model output to label

		:param onehot: 1D array with length 25
		:return: str, the label
		'''

		index = onehot.argmax()
		certainty = onehot.max() * 100
		label = self.__labels[index]

		return label, certainty

	def __predict_vgg19(self, image_path):
		'''Predicts Picnic photo with a VGG19 model

		:param image_path:
		:return: predicted label
		'''

		# load and preprocess image
		image = load_img(image_path, target_size=(224, 224))
		i_array = img_to_array(image)
		preprocessed = preprocess_input(i_array.copy())
		i = np.expand_dims(preprocessed,axis=0) / 255

		# predict
		model_output = self.__vgg19model.predict(i)

		pred, certainty = self.__onehot_to_label(model_output)

		return pred

	def predict(self, image_path):
		'''Predicts a Picnic image based on

		:param image_path:
		:return: prediction label
		'''

		# try to predict with tesseract
		found, pred = self.__tesseract.predict(image_path)
		if found:
			return pred

		# none found, so predict with VGG19 model
		pred = self.__predict_vgg19(image_path)
		return pred