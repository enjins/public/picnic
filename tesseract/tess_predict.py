'''
Project:        The Picnic Hackathon
File:           tess_predict.py
Description:    Implementation of the Tesseract prediction
Author:         Keje Sinnige <ksinnige@enjins.com>
Company:		Enjins B.V.
Creation date:  22/03/2019
Last modified:  02/04/2019
'''

from PIL import Image
import pytesseract
import csv
import pickle

WORDS_FILE = 'tesseract/words.txt'
LABEL_TO_GROUP = 'tesseract/obj/label_to_group.pkl'

class TesseractPredict:
	'''Class for predicting picnic labels using pytesseract'''

	def __init__(self):

		self.__dutch_to_subgroup = dict()
		self.__dutch = list()

		with open(WORDS_FILE,'r') as csvin:
			csvin = csv.reader(csvin, delimiter=',')
			for i,row in enumerate(csvin):
				for item in row[1:]:
					self.__dutch.append(item)
					self.__dutch_to_subgroup[item] = row[0]

		# load the subgroup to group object
		with open(LABEL_TO_GROUP, 'rb') as f:
			self.__subgroup_to_group = pickle.load(f)

	def __get_text(self,file_path):
		'''Finds the text in an image using pytesseract

		:param file_path: path to the image file
		:return: a string with the text found in the image
		'''

		im = Image.open(file_path) # read the image
		if file_path.endswith('png'):
			im = im.convert('RGBA')
		text = pytesseract.image_to_string(im)

		return text

	def predict(self,image_path):
		'''Tries to label an image with a picnic label using pytesseract

		:param image_path: path to the image (relative or absolute)
		:return found: bool indicating whether a label has been given
		:return group: picnic label. None if nothing was found
		'''

		found = False
		group = None

		# get the text in the image
		self.text = self.__get_text(image_path).lower()

		if self.text == '':
			return found, None

		found_list = list()

		# find matches
		for item in self.__dutch:
			if item in self.text:
				found = True
				subgroup = self.__dutch_to_subgroup[item]
				group = self.__subgroup_to_group[subgroup]
				found_list.append((group,self.text.index(item)))

		if found:
			best_group = found_list[0][0]
			best_index = found_list[0][1]
			for item in found_list[1:]:
				if item[1] < best_index:
					best_group = item[0]
					best_index = item[1]
			group = best_group

		return found, group