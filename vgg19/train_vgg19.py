'''
Project:        The Picnic Hackathon
File:           main.py
Description:    Training of a VGG19 model
Author:         Lars Suanet <lsuanet@enjins.com>
Company:		Enjins B.V.
Creation date:  29/03/2019
Last modified:  11/04/2019
'''

print('Importing libraries...')

import numpy as np
import pandas as pd

from keras.applications.vgg19 import VGG19, preprocess_input
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D

from keras.preprocessing import image
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, ModelCheckpoint

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

print('Done.')

tsv_path = 'hackathon/train.tsv'

images_path = 'hackathon/train'

weights_path = 'vgg19_model.h5'

model_path = 'vgg19_model.json'

def generate_arrays_from_df(df,batch_size):

    while 1:
        images = []
        labels = []
        count = 0
        for index, row in df.iterrows():
            img = image.load_img(images_path + '/' + row['file'], target_size=(224, 224))
            i = image.img_to_array(img)
            preprocessed = preprocess_input(i.copy())
            images.append(preprocessed)
            y = row.drop(['label','file']).values
            labels.append(y)

            if count % batch_size == batch_size-1:

                yield (np.array(images)/255, np.array(labels))
                images = []
                labels = []
            count += 1
        yield (np.array(images),np.array(labels))

def create_model():

    base_model = VGG19(include_top=False, weights = 'imagenet', input_shape = (224,224,3))

    x = base_model.output

    x = GlobalAveragePooling2D()(x)

    x = Dense(256, activation='relu')(x)

    predictions = Dense(NUM_CLASSES, activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=predictions)

    for layer in base_model.layers:
        layer.trainable = False

    model.summary()

    return model

# load data
all_data = pd.read_csv(tsv_path, delimiter='\t')
y_categorical = pd.get_dummies(all_data['label'])
one_hot_encoded = all_data.join(y_categorical)

image_amount = all_data.shape[0]
print('Total amount of images: ' + str(image_amount))

# training parameters
TEST_SIZE = 0.1
NUM_CLASSES = 25
VGG19_POOLING_AVERAGE = 'avg'
OBJECTIVE_FUNCTION = 'categorical_crossentropy'
LOSS_METRICS = ['accuracy']
NUM_EPOCHS = 100
EARLY_STOP_PATIENCE = 3
BATCH_SIZE_TRAINING = 32
BATCH_SIZE_VALIDATION = 32
STEPS_PER_EPOCH_TRAINING = int(image_amount*(1-TEST_SIZE))/BATCH_SIZE_TRAINING
STEPS_PER_EPOCH_VALIDATION = int(TEST_SIZE*image_amount)/BATCH_SIZE_VALIDATION

# split into train and test
train, test = train_test_split(one_hot_encoded, test_size=TEST_SIZE)

print('Initialising model...')

model = create_model()

# save model
model_json = model.to_json()
with open(model_path, "w") as json_file:
    json_file.write(model_json)
# print("Saved model to disk")

model.compile(optimizer='adam', loss=OBJECTIVE_FUNCTION, metrics=LOSS_METRICS)

print('Done.')

print('Start training...')

early_stopping = EarlyStopping(monitor='val_acc', patience=5, mode='auto', verbose=2)
model_checkpoint = ModelCheckpoint(weights_path, monitor='val_acc', save_best_only=True, verbose=2)
callbacks = [early_stopping, model_checkpoint]

model.fit_generator(generate_arrays_from_df(train,BATCH_SIZE_TRAINING),
                    validation_data=generate_arrays_from_df(test,BATCH_SIZE_VALIDATION),
                    epochs=NUM_EPOCHS, steps_per_epoch=STEPS_PER_EPOCH_TRAINING,
                    callbacks=callbacks,validation_steps=STEPS_PER_EPOCH_VALIDATION)
